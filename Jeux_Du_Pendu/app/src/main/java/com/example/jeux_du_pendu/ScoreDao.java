package com.example.jeux_du_pendu;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ScoreDao {
    @Insert
    void insert(Score score);

    @Query("DELETE FROM score_table")
    void delete_All();

    @Query("SELECT * FROM score_table ORDER BY score ASC")
    LiveData<List<Score>> getAllScore();

    @Query("SELECT count(*) FROM score_table")
    int nbElements();
}
