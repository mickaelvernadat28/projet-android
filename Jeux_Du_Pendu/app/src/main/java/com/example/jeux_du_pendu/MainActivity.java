package com.example.jeux_du_pendu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.Closeable;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void Jouer(View view){
        finish();

        Intent intent = new Intent(this, Jeu.class);
        startActivity(intent);
    }

    public void QuitterAppli(View view){
        finish();
    }

    public void Score(View view){
        Intent intent = new Intent(this,ScoreActivity.class);
        startActivity(intent);
    }
}
