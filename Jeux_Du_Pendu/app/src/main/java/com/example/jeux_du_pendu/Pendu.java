package com.example.jeux_du_pendu;

public class Pendu {

    public Pendu(){

    }

    public int VerificationLettre(String lettre, String motATrouver){
        int res = -1;
        if (lettre.length() == 1) {
            char l = lettre.charAt(0);
            for (int i = 0; i < motATrouver.length(); i++) {
                if (motATrouver.charAt(i) == l) {
                    res = i;
                }
            }
        }
        if (lettre.length() == 0){
            res = -2;
        }
        return res;
    }

    public int VerificationMot(String mot, String motATrouver){
        int res = 0;
        if (motATrouver.length() != mot.length()){
            res = -1;
        }
        else {
            res = 1;
            for (int i = 0; i < motATrouver.length(); i++) {
                if (motATrouver.charAt(i) != mot.charAt(i)){
                    res = -1;
                }
            }
        }
        return res;
    }
}
