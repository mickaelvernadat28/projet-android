package com.example.jeux_du_pendu;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

@Entity(tableName = "score_table")
public class Score {
    @PrimaryKey
    @ColumnInfo(name = "Score")
    private int score;

    public Score(int s) {
        this.score = s;
    }

    public int getScore() {
        return score;
    }
}
