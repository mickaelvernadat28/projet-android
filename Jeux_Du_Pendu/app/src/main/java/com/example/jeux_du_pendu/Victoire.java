package com.example.jeux_du_pendu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Victoire extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victoire);
        Intent i = getIntent();
        String score = i.getStringExtra("score");
        String mot = i.getStringExtra("mot");

        TextView textView = findViewById(R.id.score);
        textView.setText("score : " + score);

        TextView textView2 = findViewById(R.id.mot);
        textView2.setText("Le mot mystère était : " + mot);

    }

    public void valider(View view) {
        finish();

        Intent intent = new Intent(this,MainActivity.class);
        startActivityForResult(intent,0);
    }
}
