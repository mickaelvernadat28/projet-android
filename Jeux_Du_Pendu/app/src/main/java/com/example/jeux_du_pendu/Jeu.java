package com.example.jeux_du_pendu;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Jeu extends Activity {

    private Pendu pendu;
    private int etape;
    private int etapeMax;
    private String MotMystere;
    private String motcache;
    private ArrayList<String> listeLettre;
    private ArrayList<String> lettreBanni;
    private int score;
    private String image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        TextView txt = findViewById(R.id.motMystere);

        Resources res = getResources();
        String[] mot = res.getStringArray(R.array.MotAChoisir);

        Random r = new Random();
        int rand = r.nextInt(mot.length);
        this.MotMystere = mot[rand];

        motcache = "";
        int k = this.MotMystere.length();
        for (int i = 0; i < k; i++) {
            motcache += "_ ";
        }

        txt.setText(this.motcache);

        this.pendu = new Pendu();
        this.listeLettre = new ArrayList<>();
        this.lettreBanni = new ArrayList<>();
        this.score = 100 * this.MotMystere.length();
        this.etape = 0;
        this.etapeMax = 11;
        this.image = "image 0";
    }

    public void Recherche(View view){
        System.out.println(this.MotMystere);
        EditText editText = findViewById(R.id.Lettre);
        TextView etape = findViewById(R.id.etape);
        String lettre = editText.getText().toString();

        if (lettre.length() == this.MotMystere.length()) {
            if (this.pendu.VerificationMot(lettre, this.MotMystere) == 1) {
                afficherTout();
            }
        }
        else {
            int res = this.pendu.VerificationLettre(lettre,this.MotMystere);

            if (res == -1) {
                boolean ok = true;
                for (String l : this.lettreBanni){
                    if (l.charAt(0) == lettre.charAt(0)){
                        ok = false;
                    }
                }

                if (ok) {
                    this.etape += 1;
                    etape.setText("etape : " + this.etape);

                    lettreBanni.add(lettre);

                    TextView textView = findViewById(R.id.Lettres);
                    textView.setText(textView.getText().toString() + " " + lettre.charAt(0));

                    if (this.etape > this.etapeMax) {
                        defaite();
                    }

                    ImageView imageView = findViewById(R.id.etapes);
                    int id = this.etape;
                    switch (id) {
                        case 1 :
                            imageView.setImageResource(R.mipmap.etape1);
                            break;
                        case 2 :
                            imageView.setImageResource(R.mipmap.etape2);
                            break;
                        case 3 :
                            imageView.setImageResource(R.mipmap.etape3);
                            break;
                        case 4 :
                            imageView.setImageResource(R.mipmap.etape4);
                            break;
                        case 5 :
                            imageView.setImageResource(R.mipmap.etape5);
                            break;
                        case 6 :
                            imageView.setImageResource(R.mipmap.etape6);
                            break;
                        case 7 :
                            imageView.setImageResource(R.mipmap.etape7);
                            break;
                        case 8 :
                            imageView.setImageResource(R.mipmap.etape8);
                            break;
                        case 9 :
                            imageView.setImageResource(R.mipmap.etape9);
                            break;
                        case 10 :
                            imageView.setImageResource(R.mipmap.etape10);
                            break;
                        case 11 :
                            imageView.setImageResource(R.mipmap.etape11);
                            break;
                    }
                }
            }
            else {
                if (res != -2){
                    Affichage(res);
                }
            }
        }

        EditText editText1 = findViewById(R.id.Lettre);
        editText1.setText("");
    }

    public void Affichage(int test){
        char lettreTrouve = MotMystere.charAt(test);

        listeLettre.add(""+lettreTrouve);

        String motC = "";

        int k = this.MotMystere.length();
        for (int i = 0; i < k; i++){
            if (test == i){
                motC += lettreTrouve+" ";
            }
            else {

                if (listeLettre.contains(""+MotMystere.charAt(i))){
                    motC += MotMystere.charAt(i) + " ";
                }
                else {
                    motC += "_ ";
                }
            }
        }

        motcache = motC;
        TextView txt = findViewById(R.id.motMystere);
        txt.setText(motcache);
        if (motcache.contains(MotMystere)){
            victoire();
        }
    }

    public void afficherTout(){
        String motC = "";
        int k = this.MotMystere.length();

        for (int i = 0; i < k; i++){
            motC += MotMystere.charAt(i)+" ";
        }

        motcache = motC;
        TextView txt = findViewById(R.id.motMystere);
        txt.setText(motcache);
        boolean res = true;
        for (int i = 0; i < motcache.length(); i += 2){
            if (! MotMystere.contains(motcache.charAt(i)+"")){
                res = false;
            }
        }
        if (res){
            victoire();
        }
    }

    public void victoire(){
        Intent intent = new Intent(this, Victoire.class);
        intent.putExtra("score",""+score);
        intent.putExtra("mot", this.MotMystere);
        startActivityForResult(intent,0);
    }

    public void defaite(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivityForResult(intent,0);
    }

    public void Recommencer(View view){
        finish();

        Intent intent = new Intent(this, Jeu.class);
        startActivity(intent);
    }

    public void Quitter(View view){
        finish();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
