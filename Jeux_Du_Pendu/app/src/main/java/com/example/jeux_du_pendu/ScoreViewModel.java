package com.example.jeux_du_pendu;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class ScoreViewModel extends AndroidViewModel {
    private ScoreRepository repository;

    private LiveData<List<Score>> AllScores;

    public ScoreViewModel(Application application) {
        super(application);
        repository = new ScoreRepository(application);
        AllScores = repository.getAllScores();
    }

    public LiveData<List<Score>> getAllScores() {
        return AllScores;
    }

    public ScoreRepository getRepository() {
        return repository;
    }

    public void insert(Score score) {
        repository.insert(score);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public Integer getNbElements() {
        return repository.getNbElements();
    }
}
