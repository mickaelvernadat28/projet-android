package com.example.jeux_du_pendu;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class ScoreActivity extends AppCompatActivity {
    private ScoreViewModel scoreViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        scoreViewModel = ViewModelProviders.of(this).get(ScoreViewModel.class);

        scoreViewModel.getAllScores().observe(this,
                new Observer<List<Score>>(){
                    @Override
                    public void onChanged(@Nullable List<Score> scores){
                        TextView textView =findViewById(R.id.visualiserScores);
                        String res = "";
                        if (scores.size() == 0){
                            res = "Il n'y a pas de score.";
                        }
                        else {
                            for (int i = 0; i<scores.size();i++){
                                res += scores.get(i).getScore();
                            }
                        }
                        textView.setText(res);
                    }
                });
    }

    public ScoreViewModel getScoreViewModel() {
        return scoreViewModel;
    }
}
