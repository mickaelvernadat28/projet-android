package com.example.jeux_du_pendu;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class ScoreRepository {
    private ScoreDao scoreDao;
    private LiveData<List<Score>> AllScores;

    ScoreRepository(Application application) {
        ScoreRoomDatabase db = ScoreRoomDatabase.getDatabase(application);
        scoreDao = db.scoreDao();
        AllScores = scoreDao.getAllScore();
    }

    public LiveData<List<Score>> getAllScores() {
        return AllScores;
    }

    public void insert(Score score) {
        new insertAsyncTask(scoreDao).execute(score);
    }

    private static class insertAsyncTask extends AsyncTask<Score, Void, Void> {
        private ScoreDao AsyncTaskDao;

        insertAsyncTask(ScoreDao scoreDao) {
            AsyncTaskDao = scoreDao;
        }

        @Override
        protected Void doInBackground(final Score... params) {
            AsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void deleteAll() {
        new deleteAllAsyncTask(scoreDao).execute();
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private ScoreDao AsyncTaskDao;

        deleteAllAsyncTask(ScoreDao scoreDao) {
            AsyncTaskDao = scoreDao;
        }

        @Override
        protected Void doInBackground(Void... params) {
            AsyncTaskDao.delete_All();
            return null;
        }
    }

    public Integer getNbElements() {
        try {
            return new getNbElementsAsync(scoreDao).execute().get();
        }
        catch (Exception e) {
            Log.d("mestests", "problème getNbElementsAsync");
            return null;
        }
    }

    private static class getNbElementsAsync extends AsyncTask<Void, Void, Integer> {
        private ScoreDao AsyncTaskDao;

        getNbElementsAsync(ScoreDao scoreDao) {
            AsyncTaskDao = scoreDao;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return AsyncTaskDao.nbElements();
        }
    }
}
